import {combineReducers} from 'redux';
import product from "./product";


const reducer=combineReducers({
    product,
    //*** add other reducers
});

export default reducer