
const initialState={
    Breakfast:[],
    Lunch_Dinner:[]
};

let getMealType=(data)=>{
    let breakfast=[];
    let lunch_dinner=[];
    if(data && data.length>0){
        data.map((item)=>{
           if(item.mealType.title === 'Lunch & Dinner') {
               lunch_dinner.push(item);
           }else if(item.mealType.title === 'Breakfast'){
               breakfast.push(item);
           }
           return true
        });
        return {breakfast,lunch_dinner};
    }
};

const product=(state=initialState,action)=>{
    switch (action.type){
        case 'RECEIVE_PRODUCT':
        {
           let {breakfast,lunch_dinner}=getMealType(action.data);
            return Object.assign({}, state, {
                Breakfast: breakfast,
                Lunch_Dinner:lunch_dinner
            });
        }

        default:
            return state;
    }
};
export default product