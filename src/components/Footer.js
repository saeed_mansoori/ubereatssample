import React from 'react';
import './../assets/styles/footer.css';
class Footer extends  React.Component{

    render(){

        return(
            <div className="footer">
                <div className="footer-logo">
                <img alt="" src="https://d3i4yxtzktqr9n.cloudfront.net/web-eats-v2/12c47a69e1022b581a7e823e9bd45466.svg" />
                </div>
                <div className="footer-links">
                    <table>
                        <tbody>
                        <tr>
                            <td> About Uber Eats </td>
                            <td> Get Help</td>
                        </tr>
                        <tr>
                            <td>Read our blog</td>
                            <td>Buy gift cards</td>
                        </tr>
                        <tr>
                            <td>Buy gift cards</td>
                            <td>View all cities</td>
                        </tr>
                        <tr>
                            <td> Add your restaurant</td>
                            <td>Read FAQs</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        );
    }
}

export default Footer