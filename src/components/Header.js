import React from 'react';
import $ from 'jquery';
import './../../src/assets/styles/header.css';

class Header extends React.Component{

    constructor (...args) {
        super(...args);
        this.state = {
            height: undefined ,
            scrollAction:135
        };
        this.MainHeader = null;
        this._scrollPosition = 0;
        this.onScroll = this.onScroll.bind(this);
    }

    componentDidMount () {
        window.addEventListener('scroll', this.onScroll);
    }

    onScroll () {
        const scrollTop = window.pageYOffset || document.documentElement.scrollTop;
        if (this.state.scrollAction >= scrollTop) {
            const step = this._scrollPosition - scrollTop;
            const actualHeight = this.MainHeader.offsetHeight;
            const height = actualHeight + step;
            this.setState({ height });
            this._scrollPosition = scrollTop;
            $('#header-title').fadeIn();
            $('#logo').addClass('logo-top');
        }else{
            $('#logo').removeClass('logo-top').addClass('logo-left');
            $('#header-title').hide();

        }
    }


    render(){
        return (
            <header
                className='heroHeader'
                ref={n => this.MainHeader = n}
                style={{height: this.state.height }}>

                <div id="logo" className="logo-top">
                <img alt="" src="https://d3i4yxtzktqr9n.cloudfront.net/web-eats-v2/f8f0721f871b3704cce92eb96bc6e504.svg" />
                </div>

                <div className='heroHeader-content'>
                    <h1 id="header-title" >Restaurants you love, delivered to you</h1>
                    <div id="search" className="search-container">
                        <div className="searchbar_logo">
                        <img alt=""  src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS_Jp_xwTfbKvL8q738cicpBiSNH26lw1bnu3N2lp0JjYi5G4Xd" style={{width:30,height:30}} />
                        </div>
                        <input id="searchBar" className="searchbar" type="text" placeholder="Enter delivery address" />
                        <a href="#" id="btnSearch" className="btn-search">
                            <i className="fa fa-search">

                            </i>
                        </a>
                    </div>

                </div>
                <div className="links">
                    <a href="#">Login</a>
                    <br/>
                    <a href="#">Sign in</a>
                </div>
            </header>
        );
    }
}

export default Header