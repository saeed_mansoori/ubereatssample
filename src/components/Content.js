import React from 'react';
import './../assets/styles/content.css';
import {connect} from 'react-redux';

class Content extends  React.Component{

_renderBreakfast(){
    return(
        <div style={{marginTop:260,overflow:'hidden',padding:'0 50px'}}>

            <h3 style={{fontSize: 23,color:'#555',padding:12,marginBottom:-10}}>Breakfast</h3>
            <hr />

            {this.renderCard(this.props.breakfast)}
        </div>
    )
}

    _renderLunchDinner(){
        return(
            <div style={{overflow:'hidden'}}>
                <h3 style={{fontSize: 23,color:'#555',padding:12,display:'inline',marginBottom:-10}}>Lunch & Dinner</h3>
                <hr />
                {this.renderCard(this.props.lunch_dinner)}
            </div>
        )
    }

renderCard(data){
    return(
        <section id="breakfast" className="section-cards">

            <div style={{borderBottom:'1px solid #777'}} />
            {data.map((item,index)=> {
                    var preparation=item.preparation + item.delivery ;
                    var subTitle=item.subTitle.length < 50 ? item.subTitle :item.subTitle.substr(0,50)+'...';
                    var timeout=preparation+10;
                    var delivery=preparation +'-' +timeout +' min';
                    var rate=item.rate;
                    return(
                        <article className="card-container" key={index}>
                            <figure className="card-image">
                                <img alt="" className="image" src={item.mealType.image}/>
                            </figure>
                            <h3 className="title">{item.title}</h3>
                            <p className="text">{subTitle}</p>
                            <div id="card-footer">
                                <span className="badge">{'$ ' + item.price }</span>
                                <span className="badge badge-success">{delivery }</span>
                                <span className="badge badge-warning">{rate}</span>
                                <span className="badge badge-error">685</span>
                                <span className="badge badge-info">13</span>
                                <span className="badge badge-inverse">10</span>
                            </div>
                        </article>
                    );
                }
            )}
        </section>
    );
}

    _renderCity(){

    let city=[['Atlanta','Austin','Baltimore-Maryland','Charlotte'],['Boston','Charlotte','Chicago','Austin'],['Atlanta','Austin','Baltimore-Maryland','Charlotte'],['Boston','Charlotte','Chicago','Austin'],['Atlanta','Austin','Baltimore-Maryland','Charlotte'],['Boston','Charlotte','Chicago','Austin']];
    return(
        <div style={{padding:30}}>
            <hr />
            <div style={{padding:15}}>
        <h3>Cities Near You</h3>
        <table style={{width:'100%' }}>
            <tbody>
            {city.map((row,index)=>

        <tr key={index}>
            {row.map((city,index)=> <td key={index} className="table_data"> {city}</td>)}
        </tr>
            )}
            </tbody>
        </table >
            </div>
        </div>
    )
    }

    render(){
        return(
         <div>
             {this._renderBreakfast()}
             {this._renderLunchDinner()}
             {this._renderCity()}
         </div>
        );
    }
}

const mapStateToProps=(state)=>{
    return{
        breakfast:state.product.Breakfast,
        lunch_dinner:state.product.Lunch_Dinner
    }
};
export default connect(mapStateToProps,null)(Content)